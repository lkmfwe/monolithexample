namespace UserModule.Interface

type IUserService =
    
    abstract GetUsers : unit -> Async<UserDto seq>
    abstract AddUser : UserDto -> Async<Result<unit, string>>