namespace UserModule.Implementation.Hosting.Extensions

open Microsoft.AspNetCore.Builder
open Microsoft.Extensions.Configuration
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Logging
open System.Runtime.CompilerServices

open UserModule.Interface
open UserModule.Implementation

[<Extension>]
type UserModuleExtensions =
    [<Extension>]
    static member AddUserModule(services: IServiceCollection,
                                config: IConfiguration,
                                loggerFactory: ILoggerFactory) =
        let logger = loggerFactory.CreateLogger<UserModuleExtensions>()
        
        logger.LogInformation("Registering IUserService implementation: InMemoryUserService")
        services.AddTransient<IUserService, InMemoryUserService>() |> ignore        
        services.AddMvcCore()
            .AddJsonFormatters()
            .AddApplicationPart(typeof<UserModuleExtensions>.Assembly)
            .AddControllersAsServices()
        
    [<Extension>]
    static member UseUserModule(app: IApplicationBuilder,
                                loggerFactory: ILoggerFactory) =
        app.UseMvc()