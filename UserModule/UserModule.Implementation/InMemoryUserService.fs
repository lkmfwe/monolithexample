namespace UserModule.Implementation

open Microsoft.Extensions.Logging
open UserModule.Interface

type InMemoryUserService(log : ILogger<InMemoryUserService>) =
    
    interface IUserService with
        member x.GetUsers() = async {
            log.LogDebug("Get users")
            return [
                    { FirstName = "Ivan"; LastName = "Ivanov" }
            ] |> Seq.ofList
        }
        
        member x.AddUser user = async {
            log.LogDebug("Add user {user}", user)
            return Result.Ok ()
        }