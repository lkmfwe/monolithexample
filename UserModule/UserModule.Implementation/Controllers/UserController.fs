namespace UserModule.Implementation.Controllers

open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging

open UserModule.Interface

[<Route("/user/v1")>]
type UserController(userService:IUserService,
                    logger: ILogger<UserController>) =
    inherit ControllerBase()
    
    [<HttpGet("get")>]
    member x.GetUsers() =
        logger.LogDebug("Get users")
        userService.GetUsers()
        
    [<HttpPost("add")>]
    member x.AddUser([<FromBody>] userDto:UserDto) = async {
        let! result = userService.AddUser userDto
        match result with
        | Ok () ->
            return x.Ok() :> IActionResult
        | Error err ->
            return x.StatusCode(500, err) :> IActionResult
    }